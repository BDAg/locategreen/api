const router = require("express").Router();
const PublicacaoController = require('../controller/Publicacao');

let { uploadMulter } = require('../index');
const validator = require('../utils/validador');

router.route("/")
    .get(validator.validate, PublicacaoController.getAll)
    .post(validator.validate, PublicacaoController.post)
    .put(validator.validate, PublicacaoController.put);

router.route("/:idPost")
    .get(validator.validate, PublicacaoController.getById)
    .put(validator.validate, PublicacaoController.putById)
    .delete(validator.validate, PublicacaoController.delete)

router.route("/:idPost/uploadimage")
    .post(validator.validate, uploadMulter.single("file"), PublicacaoController.updateImage);

router.route("/feed")
    .post(validator.validate, PublicacaoController.feed);

module.exports = router;