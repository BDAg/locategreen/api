const router = require("express").Router();
const ComentarioController = require('../controller/Comentario');

const validator = require('../utils/validador');

router.route("/")
    .get(validator.validate, ComentarioController.getAll)
    .post(validator.validate, ComentarioController.post)
    .put(validator.validate, ComentarioController.put);

router.route("/:idComentario")
    .get(validator.validate, ComentarioController.getById)
    .put(validator.validate, ComentarioController.putById)
    .delete(validator.validate, ComentarioController.delete)

router.route("/findbypost/:idPost")
    .get(validator.validate, ComentarioController.findByPost);

module.exports = router;