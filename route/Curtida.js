const router = require("express").Router();
const CurtidaController = require('../controller/Curtida');

const validator = require('../utils/validador');

router.route("/")
    .get(validator.validate, CurtidaController.getAll)
    .post(validator.validate, CurtidaController.post)
    .put(validator.validate, CurtidaController.put);

router.route("/:idCurtida")
    .get(validator.validate, CurtidaController.getById)
    .put(validator.validate, CurtidaController.putById)
    .delete(validator.validate, CurtidaController.delete)

router.route("/findbypost/:idPost")
    .get(CurtidaController.findByPost);

module.exports = router;