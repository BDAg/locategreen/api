const router = require("express").Router();
const EstadoController = require('../controller/Estado');

const validator = require('../utils/validador');

router.route("/")
    .get(EstadoController.getAll)
    .post(EstadoController.post)
    .put(EstadoController.put);

router.route("/:idEstado")
    .get(validator.validate, EstadoController.getById)
    .put(EstadoController.putById)
    .delete(EstadoController.delete)

router.route("/findbycountry/:idPais")
    .get(EstadoController.findByCountry);

module.exports = router;