const router = require("express").Router();
const CidadeController = require('../controller/Cidade');

const validator = require('../utils/validador');

router.route("/")
    .get(CidadeController.getAll)
    .post(CidadeController.post)
    .put(CidadeController.put);

router.route("/:idCidade")
    .get(validator.validate, CidadeController.getById)
    .put(CidadeController.putById)
    .delete(CidadeController.delete)

router.route("/findbystate/:idEstado")
    .get(CidadeController.findByState);

module.exports = router;