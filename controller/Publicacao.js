const { Post } = require('../utils/sequelize');

const { Op } = require('sequelize');

exports.getAll = function(req, res, next) {
    Post.findAll({
        order: [ ['createdAt', 'ASC'] ]
    })
    .then(
        data => res.status(200).json(
            data
        )
    )
    .catch(
        error => {
            next(error);
        }
    )
}

exports.getById = function(req, res, next) {
    Post.findByPk(
        req.params.idPost
    )
    .then(
        data => res.status(200).json(
            data
        )
    )
    .catch(
        error => {
            next(error);
        }
    )
}

exports.post = function(req, res, next) {
    Post.create(req.body)
        .then(
            data => res.status(201).json(
                data
            )
        )
        .catch(
            error => res.status(500).json(
                {
                    "error": error.message
                }
            )
        )
}

exports.put = function(req, res, next) {
    Post.upsert(req.body)
        .then(
            data => res.status(200).json(
                data
            )
        )
        .catch(
            error => res.status(500).json(
                {
                    "error": error.message
                }
            )
        )
}

exports.putById = function(req, res, next) {

    req.body["idPost"] = req.params.idPost;

    Post.upsert(dados)
        .then(
            data => res.status(200).json(
                data
            )
        )
        .catch(
            error => res.status(500).json(
                {
                    "error": error.message
                }
            )
        )
}

exports.delete = function(req, res, next) {
    Post.destroy({
        where: {
            "idPost": req.params.idPost
        }
    })
    .then(
        data => res.status(200).json(
            data
        )
    )
    .catch(
        error => {
            next(error);
        }
    )
}

exports.updateImage = function(req, res, next) {
    
    Post.update(
        {
            imagePath: '/' + req.file.path.replace("\\", "/")
        },
        {
            where: {
                idPost: req.params.idPost
            }
        }
    )
    .then(
        data => res.status(200).json(
            data
        )
    )
    .catch(
        error => {
            res.status(500).json(
                {
                    "error": error.message
                }
            )
        }
    )

}

exports.feed = function(req, res, next) {

    let params = calcRange(req.body.lat, req.body.long, req.body.radius);

    console.log(params);

    Post.findAll({
        where: {
            lat: {
                [Op.between]: [ params['minLat'], params['maxLat'] ]
            },
            long: {
                [Op.between]: [ params['minLon'], params['maxLon'] ]
            }
        },
        order: [ [ 'createdAt', 'ASC' ] ]
    })
    .then(
        data => res.status(200).json(
            data
        )
    )
    .catch(
        error => {
            next(error);
        }
    )

}

let calcRange = (latitude, longitude, range) => {

    const R = 6371e3; // earth's mean radius in metres
    const sin = Math.sin, cos=Math.cos, acos = Math.acos;
    const π = Math.PI;

    let lat = Number(latitude);
    let long = Number(longitude);
    let radius = Number(range);

    let params = {
        minLat: lat - radius/R*180/π,
        maxLat: lat + radius/R*180/π,
        minLon: long - radius/R*180/π / cos(lat*π/180),
        maxLon: long + radius/R*180/π / cos(lat*π/180),
    };

    return params

}